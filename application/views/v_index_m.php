<!DOCTYPE html>
<html>
<head>
	<link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
  	<link href="<?php echo base_url()?>assets/css/bootstrap-theme.min.css" rel="stylesheet">
  	<link href="<?php echo base_url()?>assets/css/bootstrap.css" rel="stylesheet">  
  	<link href="<?php echo base_url()?>assets/css/home.css" rel="stylesheet">  
  	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/header.css">
  	<script src="<?php echo base_url()?>assets/js/jquery.min.js"></script>
  	<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
	<title>MalangMbois</title>
</head>
<body>
	<!-- header user -->
	<nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
		<img alt="logomm" src="<?php echo base_url()?>assets/image/logomm.png" id="logo"> 
	    </div>
	    <ul class="nav navbar-nav navbar-collapse">
	        <p id="judul">MalangMbois</p>    
	    </ul>
	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav navbar-right">
	        <li><a href="<?php echo base_url()?>c_user/cariwisata">Cari Wisata</a></li>
	        <li class="active"><a href="<?php echo base_url()?>c_user"><span class="glyphicon glyphicon-home"></span>Home</a></li>  
	         <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $this->session->userdata("namalengkap");?> <span class="caret"></span></a>
		          <ul class="dropdown-menu">
		            <li><a href="<?php echo base_url()?>c_user/profile">Profile</a></li>
		            <li><a href="<?php echo base_url()?>c_user/transaksi">Transaksi</a></li>
		            <li role="separator" class="divider"></li>
		            
		            <li><a href="c_user/logout" >Logout </a></li>
		          </ul>
		        </li>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	<!-- header user-->

	<div id="home">
	<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
	  <!-- Indicators -->
	  <ol class="carousel-indicators">
	    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
	    
	  </ol>

	  <!-- Wrapper for slides -->
	  <div class="carousel-inner" role="listbox">
	    <?php foreach ($obwis as $i) {?>
		 <div class="item active">
	      <img src="<?php echo base_url($i['foto1'])?>" alt="bromo">
	      <div class="carousel-caption">
	        <h3><span id="wisata"><?php echo $i['nama']?></span></h3>
    		<p><?php echo $i['deskripsi']?></p>
    		<a href="<?php echo base_url()?>c_user/lihatPaket/<?php echo $i['id_wisata']?>">Lihat selengkapnya</a>
	      </div>
	    </div>

	    	<?php
	    	}
	    	?>
	    
	  </div>

	  <!-- Controls -->
	  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
	    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
	    <span class="sr-only">Previous</span>
	  </a>
	  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
	    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
	    <span class="sr-only">Next</span>
	  </a>
	</div>
	</div>
</body>
</html>