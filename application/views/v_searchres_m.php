<!DOCTYPE html>
<html>
<head>
	<link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
  	<link href="<?php echo base_url()?>assets/css/bootstrap-theme.min.css" rel="stylesheet">
  	<link href="<?php echo base_url()?>assets/css/bootstrap.css" rel="stylesheet">  
  	<link href="<?php echo base_url()?>assets/css/home.css" rel="stylesheet">  
  	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/header.css">
  	<script src="<?php echo base_url()?>assets/js/jquery.min.js"></script>
  	<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
	<title>MalangMbois</title>
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- //for-mobile-apps -->
	
	<link href="<?php echo base_url()?>assets/css/style.css" rel="stylesheet" type="text/css" media="all" />
	<!-- font-awesome icons -->
	<link href="<?php echo base_url()?>assets/css/font-awesome.css" rel="stylesheet"> 
	<!-- //font-awesome icons -->
	<!-- js -->
	<script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js"></script>
	<!-- //js -->
	<link href='//fonts.googleapis.com/css?family=Raleway:400,100,100italic,200,200italic,300,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
	<!-- start-smoth-scrolling -->
	<script type="text/javascript" src="<?php echo base_url()?>assets/js/move-top.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/js/easing.js"></script>
</head>
<body>
	<!-- header user -->
	<nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
		<img alt="logomm" src="<?php echo base_url()?>assets/image/logomm.png" id="logo"> 
	    </div>
	    <ul class="nav navbar-nav navbar-collapse">
	        <p id="judul">MalangMbois</p>    
	    </ul>
	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav navbar-right">
	        <li><a href="<?php echo base_url()?>c_user/cariwisata">Cari Wisata</a></li>
	        <li class=""><a href="<?php echo base_url()?>c_user"><span class="glyphicon glyphicon-home"></span>Home</a></li>  
	         <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $this->session->userdata("namalengkap");?> <span class="caret"></span></a>
		          <ul class="dropdown-menu">
		            <li><a href="<?php echo base_url()?>c_user/profile">Profile</a></li>
		            <li><a href="<?php echo base_url()?>c_user/transaksi">Transaksi</a></li>
		            <li role="separator" class="divider"></li>
		            
		            <li><a href="<?php echo base_url()?>c_user/logout" >Logout </a></li>
		          </ul>
		        </li>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	<!-- header user-->

	<div class="container">
	    <div class="row">    

	        <div class="col-xs-8 col-xs-offset-2">
	        	<center><h2>Hasil search</h2>
	        	</center>
	        </div>
		</div>
		<?php foreach($hasil as $i){?>
		<div class="row" style="margin-top: 20px">    
	        <div class="col-xs-8 col-xs-offset-2">
			    <div class="col-md-4 agileinfo_single_left">
					<img id="example" src="<?php echo base_url()?>assets/image/bromo.jpg" alt=" " class="img-responsive">
				</div>
				<div class="col-md-8 agileinfo_single_right" style="margin-top: 30px">
				<h2><a href="<?php echo base_url('c_user/lihatPaket/');echo $i['id_wisata']?>"><?php echo $i['nama']?></a></h2>
					
					<div class="snipcart-item block" style="margin-top: 30px">
						<div class="snipcart-thumb agileinfo_single_right_snipcart">
							<h4 class="m-sing">Rp<?php echo $i['harga']?> </h4>
						</div>
						
					</div>
				</div>
	        </div>
		</div>
		<?php }?>
	</div>

	<script>
		$(document).ready(function(e){
		    $('.search-panel .dropdown-menu').find('a').click(function(e) {
				e.preventDefault();
				var param = $(this).attr("href").replace("#","");
				var concept = $(this).text();
				$('.search-panel span#search_concept').text(concept);
				$('.input-group #search_param').val(param);
			});
		});
	</script>
	
</body>
</html>