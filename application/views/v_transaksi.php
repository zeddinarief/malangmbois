<!DOCTYPE html>
<html>
<head>
  <link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/bootstrap-theme.min.css" rel="stylesheet">
    <link href="<?php echo base_url()?>assets/css/bootstrap.css" rel="stylesheet">  
    <link href="<?php echo base_url()?>assets/css/home.css" rel="stylesheet">  
    <link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/header.css">
    <script src="<?php echo base_url()?>assets/js/jquery.min.js"></script>
    <script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
    
  <title>Transaksi | MalangMbois</title>

  
</head>
<body>
  <!-- header user -->
  <nav class="navbar navbar-default">
    <div class="container-fluid">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
    <img alt="logomm" src="<?php echo base_url()?>assets/image/logomm.png" id="logo"> 
      </div>
      <ul class="nav navbar-nav navbar-collapse">
          <p id="judul">MalangMbois</p>    
      </ul>
      <!-- Collect the nav links, forms, and other content for toggling -->
      <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="<?php echo base_url()?>c_user/cariwisata">Cari Wisata</a></li>
          <li class=""><a href="<?php echo base_url()?>c_user"><span class="glyphicon glyphicon-home"></span>Home</a></li>  
           <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $this->session->userdata("namalengkap");?> <span class="caret"></span></a>
              <ul class="dropdown-menu">
                <li><a href="<?php echo base_url()?>c_user/profile">Profile</a></li>
                <li><a href="<?php echo base_url()?>c_user/transaksi">Transaksi</a></li>
                <li role="separator" class="divider"></li>
                
                <li><a href="<?php echo base_url()?>c_user/logout" >Logout </a></li>
              </ul>
            </li>
        </ul>
      </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
  </nav>
  <!-- header user-->

  <div class="container" style="margin-bottom: 30px">
        <h1 style="text-align: center;margin-top: -5px;"><b><u>Transaksi</u></b></h1>
        <div class="table-responsive">
   <table class="table table-bordered table-striped">
    <tr>
     <th class="col-md-1">No</th>
     <th class="col-md-1">id transaksi</th>
     <th class="col-md-2">Judul</th>
     <th class="col-md-1">Tanggal Pinjam</th>
     <th class="col-md-2"></th>
    </tr>
    <?php
                 $no = 1;
                 foreach ($transaksi as $data){  
                  
             ?>
    <tr>
     <th class="col-md-1"><?php echo $no++ ?></th>
     <th class="col-md-1"><?php echo $data['id_transaksi']; ?></th>
     <th class="col-md-2"><?php echo $data['nama']; ?></th>
     <th class="col-md-1"><?php echo $data['tanggal']; ?></th>
     <th class="col-md-2">
     <!-- 
     <button type="submit" class="btn btn-danger" id="tbt" onclick="return confirm('Apakah anda yakin ingin membatalkan?').href='<?php echo base_url('c_user/batalpesan'); ?>/<?php echo $data['id_transaksi']; ?>'"><h9 style="color: black">Batal</h9></button>
        <input type="button" class="btn btn-primary" id="login" value="Login" onclick="return confirm('Apakah anda yakin ingin membatalkan?').href='<?php echo base_url('c_user/batalpesan'); ?>/<?php echo $data['id_transaksi']; ?>'" /> -->
     <!-- <a class="btn btn-large btn-primary" data-toggle="confirmation" data-title="yakin ingin batal?"
   data-href="<?php echo base_url('c_user/batalpesan'); ?>/<?php echo $data['id_transaksi']; ?>" >Confirmation</a> -->
      <input type="button" class="btn btn-large btn-danger" name="delete" onclick="batalkan(<?php echo $data['id_transaksi'];?>)" value="Batal">
     </th>
    </tr>  
     <?php
      }
     ?>
    
    
   </table>
   <td colspan="6">Total Transaksi</td>
   <td>   
    <?php echo $no -= 1 ?>
   </td>
  </div>
        
 </div>
 <script type="text/javascript">
   function batalkan(id){
     if(confirm("Yakin ingin batal ?")){
        window.location.href="<?php echo base_url('c_user/batalpesan'); ?>/"+id;
     }
   }
 </script>
</body>
</html>