<!DOCTYPE html>
<html>
<head>
	<link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
  	<link href="<?php echo base_url()?>assets/css/bootstrap-theme.min.css" rel="stylesheet">
  	<link href="<?php echo base_url()?>assets/css/bootstrap.css" rel="stylesheet">  
  	<link href="<?php echo base_url()?>assets/css/home.css" rel="stylesheet">  
  	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/header.css">
  	<script src="<?php echo base_url()?>assets/js/jquery.min.js"></script>
  	<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>

  	<link rel="stylesheet" href="<?php echo base_url('assets/css/flexslider.css')?>" type="text/css" media="screen" />
	<title>MalangMbois</title>
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false);
		function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!-- //for-mobile-apps -->
	
	<link href="<?php echo base_url()?>assets/css/style.css" rel="stylesheet" type="text/css" media="all" />
	<!-- font-awesome icons -->
	<link href="<?php echo base_url()?>assets/css/font-awesome.css" rel="stylesheet"> 
	<!-- //font-awesome icons -->
	<!-- js -->
	<script src="<?php echo base_url()?>assets/js/jquery-1.11.1.min.js"></script>
	<!-- //js -->
	<link href='//fonts.googleapis.com/css?family=Raleway:400,100,100italic,200,200italic,300,400italic,500,500italic,600,600italic,700,700italic,800,800italic,900,900italic' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>
	<!-- start-smoth-scrolling -->
	<script type="text/javascript" src="<?php echo base_url()?>assets/js/move-top.js"></script>
	<script type="text/javascript" src="<?php echo base_url()?>assets/js/easing.js"></script>
	<script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
			});
		});
	</script>
</head>
<body>
	<!-- header user -->
	<nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
		<img alt="logomm" src="<?php echo base_url()?>assets/image/logomm.png" id="logo"> 
	    </div>
	    <ul class="nav navbar-nav navbar-collapse">
	        <p id="judul">MalangMbois</p>    
	    </ul>
	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav navbar-right">
	        <li><a href="<?php echo base_url()?>c_user/cariwisata">Cari Wisata</a></li>
	        <li class=""><a href="<?php echo base_url()?>c_user"><span class="glyphicon glyphicon-home"></span> Home</a></li>
	        <li><a href="<?php echo base_url()?>c_user/register">Registrasi</a></li>
	        <input type="button" class="btn btn-primary" id="login" value="Login" onclick="location.href='<?php echo base_url()?>c_user/login'" />
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	<!-- header user-->

	<div class="breadcrumbs">
		<div class="container">
			<ol class="breadcrumb breadcrumb1 animated wow slideInLeft" data-wow-delay=".5s">
				<li><a href="<?php echo base_url()?>c_user"><span class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>
				<li class="active">Wisata</li>
			</ol>
			
		</div>
	</div>
<!-- //breadcrumbs -->
	<div class="products">
		<div class="container">
			<div class="agileinfo_single">
				<?php foreach ($detailobwis as $i) {?>
					
				<div class="col-md-4 single-right-left ">
			       <div class="grid images_3_of_2">
			        <div class="flexslider">

			         <ul class="slides">
			          <li data-thumb="<?php echo base_url($i['foto1']);?>">
			           <div class="thumb-image"> <img src="<?php echo base_url($i['foto1']);?>" data-imagezoom="true" class="img-responsive"> </div>
			          </li>
			          <li data-thumb="<?php echo base_url($i['foto2']);?>">
			           <div class="thumb-image"> <img src="<?php echo base_url($i['foto2']);?>" data-imagezoom="true" class="img-responsive"> </div>
			          </li>
			          <li data-thumb="<?php echo base_url($i['foto3']);?>">
			           <div class="thumb-image"> <img src="<?php echo base_url($i['foto3']);?>" data-imagezoom="true" class="img-responsive"> </div>
			          </li>
			         </ul>
			         <div class="clearfix"></div>
			        </div>
			       </div>
			      </div>
				<div class="col-md-8 agileinfo_single_right">
				<h2><?php echo $i['nama'];?></h2>
					
					<div class="w3agile_description">
						<h4>Description :</h4>
						<p><?php echo $i['deskripsi'];?></p>
					</div>
					<div class="snipcart-item block">
						<div class="snipcart-thumb agileinfo_single_right_snipcart">
							<h4 class="m-sing">Rp<?php echo $i['harga'];?> </h4>
						</div>
						<div class="snipcart-thumb agileinfo_single_right_snipcart" >
							<h4 class="m-sing"><?php echo $i['status']?></h4>
						</div>
						<div class="snipcart-details agileinfo_single_right_details">
							<form action="<?php echo base_url('c_user/pesan/');echo $i['id_wisata'];?>" method="POST">
								
								<input type="submit" name="submit" value="Pesan" class="button">
								
							</form>
						</div>
					</div>
				</div>
				<div class="clearfix"> </div>
				<?php } ?>
			</div>
		</div>
	</div>

	<!-- top-header and slider -->
	<!-- here stars scrolling icon -->
	<script type="text/javascript">
		$(document).ready(function() {
			/*
				var defaults = {
				containerID: 'toTop', // fading element id
				containerHoverID: 'toTopHover', // fading element hover id
				scrollSpeed: 1200,
				easingType: 'linear' 
				};
			*/
								
			$().UItoTop({ easingType: 'easeOutQuart' });
								
			});
	</script>
	<!-- //here ends scrolling icon -->
	<script src="<?php echo base_url()?>assets/js/minicart.min.js"></script>
	<script>
		// Mini Cart
		paypal.minicart.render({
			action: '#'
		});

		if (~window.location.search.indexOf('reset=true')) {
			paypal.minicart.reset();
		}
	</script>
	<!-- main slider-banner -->
	<script src="<?php echo base_url()?>assets/js/skdslider.min.js"></script>
	<link href="<?php echo base_url()?>assets/css/skdslider.css" rel="stylesheet">
	<script type="text/javascript">
			jQuery(document).ready(function(){
				jQuery('#demo1').skdslider({'delay':5000, 'animationSpeed': 2000,'showNextPrev':true,'showPlayButton':true,'autoSlide':true,'animationType':'fading'});
							
				jQuery('#responsive').change(function(){
				  $('#responsive_wrapper').width(jQuery(this).val());
				});
				
			});
	</script>	

	<script src="<?php echo base_url('assets/js/jquery.flexslider.js')?>"></script>
          <script>
          // Can also be used with $(document).ready()
           $(window).load(function() {
            $('.flexslider').flexslider({
            animation: "slide",
            controlNav: "thumbnails"
            });
           });
          </script>

</body>
</html>