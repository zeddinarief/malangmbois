<!DOCTYPE html>
<html>
<head>
  <link href="<?php echo base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/css/bootstrap-theme.min.css')?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/css/bootstrap.css')?>" rel="stylesheet">  
  <link href="<?php echo base_url('assets/css/login.css')?>" rel="stylesheet">  
  <script src="<?php echo base_url('assets/js/jquery.min.js')?>"></script>
  <script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
	<title>ADMIN Login | MalangMbois</title>
</head>
<body>
<div id="login">
<form action="<?php echo base_url('c_login/adminlogin')?>" class="form-horizontal" method="POST">
  <b class="admin">Admin</b>
  <img src="<?php echo site_url('assets/image/logomm.png'); ?> " id="logo" class="col-sm-offset-0 ">
  <div class="form-group">
    <label for="inputusername" class="col-sm-4 control-label">Username</label>
    <div class="col-sm-6">
      <input type="text" name="username" class="form-control"  placeholder="Username">
    </div>
  </div>
  <div class="form-group">
    <label for="inputPassword" class="col-sm-4 control-label">Password</label>
    <div class="col-sm-6">
      <input type="password" name="password" class="form-control"  placeholder="Password">
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-4 col-sm-10">
      <button type="submit" class="btn btn-default">Sign in</button>
    </div>
  </div>
</form> 
</div>
</body>
</html>