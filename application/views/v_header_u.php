<!DOCTYPE html>
<html>
<head>
  <link href="assets/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/css/bootstrap-theme.min.css" rel="stylesheet">
  <link href="assets/css/bootstrap.css" rel="stylesheet">  
  <link rel="stylesheet" type="text/css" href="assets/css/header.css">
  <script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>
  <title>MalangMbois</title>
</head>
<body>
<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <img alt="logomm" src="assets/image/logomm.png" id="logo"> 
    </div>
    <ul class="nav navbar-nav navbar-collapse">
        <p id="judul">MalangMbois</p>    
    </ul>
    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#">Cari Wisata</a></li>
        <li class="active"><a href="#"><span class="glyphicon glyphicon-home"></span> Home</a></li>
        <li><a href="#">Registrasi</a></li>
        <a id="login" href="<?php echo site_url('c_user/login');?>" >Login</a>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
</body>
</html>

  
