<!DOCTYPE html>
<html>
<head>
  <link href="<?php echo base_url('assets/css/bootstrap.min.css')?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/css/bootstrap-theme.min.css')?>" rel="stylesheet">
  <link href="<?php echo base_url('assets/css/bootstrap.css')?>" rel="stylesheet">  
  <link href="<?php echo base_url('assets/css/login.css')?>" rel="stylesheet">  
  <script src="<?php echo base_url('assets/js/jquery.min.js')?>"></script>
  <script src="<?php echo base_url('assets/js/bootstrap.min.js')?>"></script>
	<title>Register | MalangMbois</title>
</head>
<body>
<div id="regis">
<form action="<?php echo base_url('c_user/aksi_regis')?>" class="form-horizontal" method="POST">
  <img src="<?php echo site_url('assets/image/logomm.png'); ?> " id="logo" class="col-sm-offset-4 ">
  <div class="form-group">
    <label for="inputnama" class="col-sm-4 control-label">Nama Lengkap</label>
    <div class="col-sm-6">
      <input type="text" name="nama" class="form-control" placeholder="Nama Lengkap" required="">
    </div>
  </div>
  <div class="form-group">
    <label for="inputusername" class="col-sm-4 control-label">Username</label>
    <div class="col-sm-6">
      <input type="text" name="username" id="inputusername" class="form-control" placeholder="Username" required="">
    </div>
    <span id="user_check" style="margin-left: 200px"></span>
  </div>

  <div class="form-group">
    <label for="inputPassword" class="col-sm-4 control-label">Password</label>
    <div class="col-sm-6">
      <input type="password" name="password" class="form-control" placeholder="Password" required="">
    </div>
  </div>
  <div class="form-group">
    <label for="inputconfirmpass" class="col-sm-4 control-label">Confirm Password</label>
    <div class="col-sm-6">
      <input type="password" name="confirmpass" class="form-control" placeholder="Confirm password" required="">
    </div>
  </div>
  <div class="form-group">
    <label for="inputemail" class="col-sm-4 control-label">Email</label>
    <div class="col-sm-6">
      <input type="email" name="email" class="form-control" placeholder="Email" required="">
    </div>
  </div>
  <div class="form-group">
    <label for="inputtelp" class="col-sm-4 control-label">No. Telp</label>
    <div class="col-sm-6">
      <input type="text" name="telp" class="form-control" placeholder="No. telp" required="">
    </div>
  </div>
  <div class="form-group">
    <label for="inputjk" class="col-sm-4 control-label">Jenis Kelamin</label>
    <div class="col-sm-6">
      <input type="radio" name="gender" value="pria"> Pria<br>
      <input type="radio" name="gender" value="wanita"> Wanita
    </div>
  </div>
  <div class="form-group">
    <label for="inputtelp" class="col-sm-4 control-label">Alamat</label>
    <div class="col-sm-6">
      <textarea type="text" name="alamat" class="form-control" placeholder="Alamat lengkap"  required=""></textarea> 
    </div>
  </div>
  <div class="form-group">
    <div class="col-sm-offset-4 col-sm-10">
      <button type="submit" class="btn btn-default">Register</button>
    </div>
  </div>
</form> 
</div>
<script type="text/javascript">
 $(document).ready(function(){  
        $('#inputusername').change(function(){  
            var username = $('#inputusername').val();  
            if(username != '') {  
                $.ajax({  
                    url:"<?php echo base_url('c_user/cek_ketersediaan_user'); ?>",  
                    method:"POST",  
                    data:{username},  
                    success:function(data){ 
                        $('#user_check').html(data);  
                    }  
                });  
            }
            
        });  
    });  
</script>
</body>
</html>