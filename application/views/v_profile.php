<!DOCTYPE html>
<html>
<head>
	<link href="<?php echo base_url()?>assets/css/bootstrap.min.css" rel="stylesheet">
  	<link href="<?php echo base_url()?>assets/css/bootstrap-theme.min.css" rel="stylesheet">
  	<link href="<?php echo base_url()?>assets/css/bootstrap.css" rel="stylesheet">  
  	<link href="<?php echo base_url()?>assets/css/home.css" rel="stylesheet">  
  	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>assets/css/header.css">
  	<script src="<?php echo base_url()?>assets/js/jquery.min.js"></script>
  	<script src="<?php echo base_url()?>assets/js/bootstrap.min.js"></script>
	<title>Profile | MalangMbois</title>
</head>
<body>
	<!-- header user -->
	<nav class="navbar navbar-default">
	  <div class="container-fluid">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
		<img alt="logomm" src="<?php echo base_url()?>assets/image/logomm.png" id="logo"> 
	    </div>
	    <ul class="nav navbar-nav navbar-collapse">
	        <p id="judul">MalangMbois</p>    
	    </ul>
	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav navbar-right">
	        <li><a href="<?php echo base_url()?>c_user/cariwisata">Cari Wisata</a></li>
	        <li class=""><a href="<?php echo base_url()?>c_user"><span class="glyphicon glyphicon-home"></span>Home</a></li>  
	         <li class="dropdown">
		          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $this->session->userdata("namalengkap");?> <span class="caret"></span></a>
		          <ul class="dropdown-menu">
		            <li><a href="<?php echo base_url()?>c_user/profile">Profile</a></li>
		            <li><a href="<?php echo base_url()?>c_user/transaksi">Transaksi</a></li>
		            <li role="separator" class="divider"></li>
		            
		            <li><a href="<?php echo base_url()?>c_user/logout" >Logout </a></li>
		          </ul>
		        </li>
	      </ul>
	    </div><!-- /.navbar-collapse -->
	  </div><!-- /.container-fluid -->
	</nav>
	<!-- header user-->

	<div class="container">
	    <h1>Profile</h1>
	  	<hr>
		<div class="row">
	      <!-- left column -->
	      
	      
	      <!-- edit form column -->
	      <div class="col-md-12 personal-info">
	        <!-- <div class="alert alert-info alert-dismissable">
	          <a class="panel-close close" data-dismiss="alert">×</a> 
	          <i class="fa fa-coffee"></i>
	          This is an <strong>.alert</strong>. Use this to show important messages to the user.
	        </div>
	         -->
	        <form action="<?php echo base_url()?>c_user/editprofile" method="POST" class="form-horizontal" role="form">
	          <h3>Data Diri</h1>
	          <div class="form-group">
	            <label class="col-lg-3 control-label">Nama</label>
	            <div class="col-lg-8">
	              <input class="form-control" name="nama" value="<?php echo $this->session->userdata('namalengkap')?>" type="text">
	            </div>
	          </div>

	          <div class="form-group">
	            <label class="col-lg-3 control-label">Username</label>
	            <div class="col-lg-8">
	              <input class="form-control" name="username" value="<?php echo $this->session->userdata('user')?>" type="text">
	            </div>
	          </div>
	          
	          <div class="form-group">
	            <label class="col-md-3 control-label">email:</label>
	            <div class="col-md-8">
	              <input class="form-control" name="email" value="<?php echo $this->session->userdata('email')?>" type="email">
	            </div>
	          </div>
	          
	          <div class="form-group">
	            <label class="col-lg-3 control-label">No. telp</label>
	            <div class="col-lg-8">
	              <input class="form-control" name="telp" value="0<?php echo $this->session->userdata('telp')?>" type="text">
	            </div>
	          </div>

	          <div class="form-group">
	            <label class="col-lg-3 control-label">Alamat</label>
	            <div class="col-lg-8">
	              <input class="form-control" name="alamat" value="<?php echo $this->session->userdata('alamat')?>" type="text">
	            </div>
	          </div>

	          <div class="form-group">
	            <label class="col-md-3 control-label"></label>
	            <div class="col-md-8">
	              <input class="btn btn-primary" value="Save Changes" type="submit">
	              <span></span>
	              <input class="btn btn-default" value="Cancel" type="reset">
	            </div>
	          </div>
	        </form>

	        <form action="<?php echo base_url()?>c_user/editpass" method="POST" class="form-horizontal" role="form">
	        	<h3>Ganti Password</h3>
	        	<div class="form-group">
	            <label class="col-md-3 control-label">Old Password:</label>
	            <div class="col-md-8">
	              <input class="form-control" name="oldpassword" type="password">
	            </div>
	          </div>
	          <div class="form-group">
	            <label class="col-md-3 control-label">New Password:</label>
	            <div class="col-md-8">
	              <input class="form-control" name="newpassword" type="password">
	            </div>
	          </div>
	          <div class="form-group">
	            <label class="col-md-3 control-label"></label>
	            <div class="col-md-8">
	              <input class="btn btn-primary" value="Save Changes" type="submit">
	              <span></span>
	              <input class="btn btn-default" value="Cancel" type="reset">
	            </div>
	          </div>
	        </form>
	      </div>
	  </div>
	</div>
</body>
</html>