<?php
	class C_admin extends CI_Controller {
		function __construct(){
			parent::__construct();
		}

		function logoutadmin(){
			$this->session->sess_destroy();
			redirect(base_url("c_admin/admin"));
		}

		function admin(){
			if($this->session->userdata('statusadmin') != "logged"){
				$this->load->view('v_loginadmin');	
			}else{
				$this->load->view('admin/v_indexadmin');

			}

		}

		function daftarwisata(){
			$this->wisata['obwis'] = $this->m_wisata->tampilobwis('objekwisata');
			$this->load->view('v_wisata',$this->wisata);
			
		}

		function daftarmember(){
			$this->src['member'] = $this->m_user->lihatmember('member');
			$this->load->view('v_daftarmember',$this->src);
		}

		function transaksi(){
			$this->src['transaksi'] = $this->m_user->lihatsemuatransaksi('transaksiobwis');
			$this->load->view('v_transaksi_a',$this->src);	
		}

		function tambahwisata(){
			$nama = $this->input->POST('nama');
			  $username = $this->input->POST('username');
			  $password = $this->input->POST('password');
			  $confirmpass = $this->input->POST('confirmpass');
			  $email = $this->input->POST('email');
			  $telp = $this->input->POST('telp');
			  $gender = $this->input->POST('gender');
			  $alamat = $this->input->POST('alamat');

			  $where = array(
				'username' => $username,
				'password' => $password
				);
			  
			  $data = array( 
			   'nama' => $nama,
			   'deskripsi' => $deskripsi,
			   'kategr' => $email,
			   'password' => $password,
			   'no_telp' => $telp,
			   'jeniskelamin' => $gender,
			   'alamat' => $alamat
			   );

			  if ($password != $confirmpass) {
			  		echo '<script type="text/javascript">alert("password tidak cocok !!");</script>';
			  		$this->load->view('v_register');
			  	# code...
			  }	
			  else{
			  	$this->load->model("m_user");  
				$this->m_user->insertMember($data,'member');
				$a = $this->m_user->selectmember("member",$where)->result_array();
			  	foreach ($a as $d) {}
				$data_session = array(
					'id' => $d['id_member'],
			    	'user' => $username,
			    	'password' => $password,
					'namalengkap' => $nama,
					'alamat' => $alamat,
					'telp' => $telp,
					'email' => $email,
					'jeniskelamin' => $gender,
					'status' => "logged"
					);
		 		
				$this->session->set_userdata($data_session);
		 
				redirect(base_url("c_user"));
			  }
		}

	}