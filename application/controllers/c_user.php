<?php
	class C_user extends CI_Controller {
		function __construct(){
			parent::__construct();			
			$this->load->model('m_wisata');
			}
		
		function index(){
			$this->wisata['obwis'] = $this->m_wisata->tampilhome('objekwisata');
			
			if($this->session->userdata('status') != "logged"){
				$this->load->view('v_index',$this->wisata);
			}else{
				$this->load->view('v_index_m',$this->wisata);
			}
		}

		function login(){
			$this->load->view('v_login');
		}
		function logout(){
			$this->session->sess_destroy();
			redirect(base_url("c_user"));
		}

		function register(){
			$this->load->view('v_register');	
		} 
		
		function cek_ketersediaan_user(){
	     	$where = array(
				'username' => $_POST["username"],
				
				);
	            $this->load->model("m_user");  
	            
	            if($this->m_user->is_user_available($where)) {  
	                echo '<label class="text-danger"><span class="glyphicon glyphicon-remove"></span> Username Sudah Terdaftar</label>';  
	            }
	            else {  
	                echo '<label class="text-success"><span class="glyphicon glyphicon-ok"></span> Username Tersedia</label>';  
	            }  
	 	}

	 	function aksi_regis(){
			  $nama = $this->input->POST('nama');
			  $username = $this->input->POST('username');
			  $password = $this->input->POST('password');
			  $confirmpass = $this->input->POST('confirmpass');
			  $email = $this->input->POST('email');
			  $telp = $this->input->POST('telp');
			  $gender = $this->input->POST('gender');
			  $alamat = $this->input->POST('alamat');

			  $where = array(
				'username' => $username,
				'password' => $password
				);
			  
			  $data = array( 
			   'nama' => $nama,
			   'username' => $username,
			   'email' => $email,
			   'password' => $password,
			   'no_telp' => $telp,
			   'jeniskelamin' => $gender,
			   'alamat' => $alamat
			   );

			  if ($password != $confirmpass) {
			  		echo '<script type="text/javascript">alert("password tidak cocok !!");</script>';
			  		$this->load->view('v_register');
			  	# code...
			  }	
			  else{
			  	$this->load->model("m_user");  
				$this->m_user->insertMember($data,'member');
				$a = $this->m_user->selectmember("member",$where)->result_array();
			  	foreach ($a as $d) {}
				$data_session = array(
					'id' => $d['id_member'],
			    	'user' => $username,
			    	'password' => $password,
					'namalengkap' => $nama,
					'alamat' => $alamat,
					'telp' => $telp,
					'email' => $email,
					'jeniskelamin' => $gender,
					'status' => "logged"
					);
		 		
				$this->session->set_userdata($data_session);
		 
				redirect(base_url("c_user"));
			  }

		}

		function lihatPaket($id){
			$where = array (
				'id_wisata' => $id
				);
			$this->wisata['detailobwis'] = $this->m_wisata->tampildetailobwis('objekwisata',$where);
			if($this->session->userdata('status') != "logged"){
				$this->load->view('v_paketwisata',$this->wisata);
			}else{
				$this->load->view('v_paketwisata_m',$this->wisata);
			}			
		}

		function profile(){
			$this->load->view('v_profile');
		}

		function editprofile(){
			  $nama = $this->input->POST('nama');
			  $username = $this->input->POST('username');
			  $email = $this->input->POST('email');
			  $telp = $this->input->POST('telp');
			  $alamat = $this->input->POST('alamat');
			  
			  $data = array(
			   'nama' => $nama,
			   'username' => $username,
			   'email' => $email,
			   'no_telp' => $telp,
			   'alamat' => $alamat,
			   );				

			   $id = $this->session->userdata('id'); 
			   $where = array(
				'id_member' => $id
				);
			  	$this->load->model("m_user");  
				$this->m_user->updateMember($where,$data,'member');
				$a = $this->m_user->selectmember("member",$where)->result_array();
				foreach ($a as $d) {}
				$data_session = array(
					'id' => $id,
			    	'user' => $username,
					'namalengkap' => $nama,
					'alamat' => $alamat,
					'password' => $d['password'],
					'email' => $email,
					'telp' => $telp,
					'jeniskelamin' => $d['jeniskelamin'],
					'status' => "logged"
					);
		 		
				$this->session->set_userdata($data_session);
		 
				redirect(base_url("c_user/profile"));
		}

		function editpass(){
			  $oldpass = $this->input->POST('oldpassword');
			  $newpass = $this->input->POST('newpassword');
			  $pass = $this->session->userdata('password'); 
			  
			  if ($oldpass != $pass) {
			  	echo '<script type="text/javascript">alert("password lama anda salah !!");</script>';
			  		$this->load->view('profilemember');
			  	# code...
			  }
			  else{
				  $data = array(
				   'password' => $newpass
				   );				

				   $id = $this->session->userdata('id'); 
				   $where = array(
					'id_member' => $id
					);	
				  	$this->load->model("m_user");  
					$this->m_user->updateMember($where,$data,'member');
					$a = $this->m_user->selectmember("member",$where)->result_array();
					foreach ($a as $d) {}
					$data_session = array(
						'id' => $id,
				    	'user' => $d['username'],
						'namalengkap' => $d['nama'],
						'alamat' => $d['alamat'],
						'password' => $newpass,
						'email' => $d['email'],
						'telp' => $d['no_telp'],
						'jeniskelamin' => $d['jeniskelamin'],
						'status' => "logged"
						);
			 		
					$this->session->set_userdata($data_session);

			 		echo '<script type="text/javascript">alert("password anda telah terganti !!");</script>';
					$this->load->view('profilemember');
			  }
		}

		function transaksi(){
			$this->load->model('m_user');
			$this->src['transaksi'] = $this->m_user->lihattransaksi('transaksiobwis');
			$this->load->view('v_transaksi', $this->src);
		}

		function cariwisata(){
			$this->wisata['obwis'] = $this->m_wisata->tampilobwis('objekwisata');
			if($this->session->userdata('status') != "logged"){
				$this->load->view('v_search',$this->wisata);
			}else{
				$this->load->view('v_search_m',$this->wisata);
			}		
		}

		function search(){
			$cari = $this->input->POST('search');
			$this->src['hasil'] = $this->m_wisata->cariwisata($cari);
			
			if($this->session->userdata('status') != "logged"){
				$this->load->view('v_searchres', $this->src);
			}else{
				$this->load->view('v_searchres_m', $this->src);
			}
		}

		// function find($key){
			
		// 	$this->src['hasil'] = $this->m_wisata->cariwisata($key);
		// 	$this->load->view('v_searchres', $this->src);
		// }

		function pesan($id){
			if($this->session->userdata('status') != "logged"){
				echo '<script type="text/javascript">alert("silahkan login dahulu!");</script>';
					$this->load->view('v_login');
			}else{
				$tgl = date('Y-m-d');
				$where = array(
					'id_wisata' => $id
					);
				$id_member = $this->session->userdata("id");
				$a = $this->m_wisata->tampildetailobwis("objekwisata",$where);
				
				foreach ($a as $d) {}
				$data = array(
					'id_wisata' => $id,
					'id_member' => $id_member,
					'tanggal' => $tgl,
					'total' => $d['harga']
					 );
				$this->wisata['detailobwis'] = $this->m_wisata->tampildetailobwis('objekwisata',$where);
				if ($d['status'] != 'tersedia') {
					echo '<script type="text/javascript">alert("Maaf , paket tidak tersedia!");</script>';
					$this->load->view('v_paketwisata_m',$this->wisata);
				}else{
				$this->m_wisata->pesan('transaksiobwis',$data);
				echo '<script type="text/javascript">alert("berhasil di pesan!");</script>';
					echo "<script>history.go(-1);</script>";
				}
			}
		}

		function batalpesan($id){
			$where = array(
				'id_transaksi' => $id
				);
			$this->m_wisata->batal('transaksiobwis',$where);
			echo "<script>history.go(-1);</script>";	
		}

	}