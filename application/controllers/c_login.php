<?php
	class C_login extends CI_Controller {
		function __construct(){
			parent::__construct();
			$this->load->model('m_login');

		}

		function ceklogin(){
			$username = $this->input->POST('username');
			$password = $this->input->POST('password');
			$where = array(
				'username' => $username,
				'password' => $password
				);
			$cek = $this->m_login->cek_login("member",$where)->num_rows();
			$a = $this->m_login->cek_login("member",$where)->result_array();
			
			if($cek > 0){
				foreach ($a as $d) {}
				$data_session = array(
					'id' => $d['id_member'],
					'user' => $username,
					'namalengkap' => $d['nama'],
					'password' => $d['password'],
					'email' => $d['email'],
					'alamat' => $d['alamat'],
					'telp' => $d['no_telp'],
					'jeniskelamin' => $d['jeniskelamin'],
					'status' => "logged"
					);
		 		
				$this->session->set_userdata($data_session);
		 
				redirect(base_url("c_user"));
			}else{
				echo '<script type="text/javascript">alert("username atau password salah!!");</script>';
				$this->load->view('v_login');	
			}
		}
		function adminlogin(){
			$username = $this->input->POST('username');
			$password = $this->input->POST('password');
			$where = array(
				'username' => $username,
				'password' => $password
				);
			$cek = $this->m_login->cek_login("admin",$where)->num_rows();
			$a = $this->m_login->cek_login("admin",$where)->result_array();
			if($cek > 0){
				foreach ($a as $d) {}
				$session_admin = array(
					'useradmin' => $username,
					'nama' => $d['nama'],
					'statusadmin' => "logged"
					);
		 		
				$this->session->set_userdata($session_admin);
		 
				redirect(base_url("c_admin/admin"));
			}else{
				echo '<script type="text/javascript">alert("username atau password salah!!");</script>';
				$this->load->view('v_loginadmin');	
			}
		}	
		
	}