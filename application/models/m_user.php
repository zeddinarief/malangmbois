<?php
class M_user extends CI_Model{	
	function selectmember($table,$where){		
		return $this->db->get_where($table,$where);
	}

	function is_user_available($where) {  
        
        $query = $this->db->get_where("member",$where);  
        if($query->num_rows() > 0) {  
            return true;  
        }  
        else {  
            return false;  
        }  
   }

   function insertMember($data,$table){
        $this->db->insert($table , $data);
   }

   function updateMember($where,$data,$table){
        $this->db->where($where);
        $this->db->update($table,$data);
   }

   function lihattransaksi($table){
        $query = $this->db->query("SELECT * FROM transaksiobwis JOIN objekwisata ON transaksiobwis.id_wisata=objekwisata.id_wisata");
        
        return $query->result_array();
   }

   function lihatsemuatransaksi($table){    
    return $this->db->get($table);
  }

  function lihatmember($table){   
    return $this->db->get($table);
  }

}