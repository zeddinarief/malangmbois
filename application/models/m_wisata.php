<?php
class M_wisata extends CI_Model{	
	
	function tampilhome($table){
		$this->db->order_by('id_wisata', 'DESC');
		$this->db->limit(3);
		$query = $this->db->get($table);
		return $query->result_array();
	}

	function tampildetailobwis($table,$where){
		$query = $this->db->get_where($table, $where);
		return $query->result_array();
	}

	function tampilobwis($table){
		$this->db->order_by('id_wisata', 'DESC');
		$query = $this->db->get($table);
		return $query->result_array();
	}

	function cariwisata($key){
		$query = $this->db->query("SELECT * FROM objekwisata  WHERE nama LIKE '%$key%'");
        return $query->result_array();
	}

	function pesan($table,$data){
		$this->db->insert($table , $data);
	}

	function batal($table,$where){
		$this->db->where($where);
		$this->db->delete($table);
		
		
	}

}