-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 12, 2018 at 04:23 AM
-- Server version: 10.1.28-MariaDB
-- PHP Version: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `malangmbois`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id_admin`, `nama`, `username`, `password`) VALUES
(1, 'zeddin arief', 'zeddinarief', 'admin'),
(2, 'achmad alim', 'achmadalim15', 'admin'),
(3, 'masayu vidya', 'msyvidya', 'admin'),
(4, 'musthafani', 'musthafani', 'admin'),
(5, 'makruf puja', 'makruf', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `member`
--

CREATE TABLE `member` (
  `id_member` int(11) NOT NULL,
  `nama` varchar(30) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `email` varchar(20) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `jeniskelamin` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `member`
--

INSERT INTO `member` (`id_member`, `nama`, `username`, `password`, `email`, `no_telp`, `alamat`, `jeniskelamin`) VALUES
(2, 'gembong a', 'gembong', 'gembong', 'gembong@mail.com', '868686878', 'malang', 'pria'),
(3, 'mac mac', 'macmac', 'macmac', 'cacan@gmail.com', '0855353535', 'Adohhhhhh', 'pria'),
(4, 'Muhammad Ali F', 'malif', 'malif', 'msyvidya@gmail.com', '2147483647', 'Kanor', 'pria'),
(6, 'coba aja ni', 'coba', '6', 'coba@co.com', '9988767', 'cobaaja', 'pria');

-- --------------------------------------------------------

--
-- Table structure for table `objekwisata`
--

CREATE TABLE `objekwisata` (
  `id_wisata` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `deskripsi` varchar(300) NOT NULL,
  `fasilitas` varchar(300) NOT NULL,
  `status` varchar(10) NOT NULL,
  `harga` double NOT NULL,
  `kategori` varchar(10) NOT NULL,
  `foto1` varchar(100) NOT NULL,
  `foto2` varchar(100) NOT NULL,
  `foto3` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `objekwisata`
--

INSERT INTO `objekwisata` (`id_wisata`, `nama`, `deskripsi`, `fasilitas`, `status`, `harga`, `kategori`, `foto1`, `foto2`, `foto3`) VALUES
(1, 'Bromo', 'Gunung Bromo adalah sebuah gunung berapi aktif di Jawa Timur, Indonesia. Gunung ini memiliki ketinggian 2.329 meter di atas permukaan laut dan berada dalam empat wilayah kabupaten, yakni Kabupaten Probolinggo, Kabupaten Pasuruan, Kabupaten Lumajang, dan Kabupaten Malang.', 'kendaraan, tour guide , mengunjungi 3 spot', 'tersedia', 500000, 'obwis', 'assets/wisata/bromo.jpg', '', ''),
(2, 'Sendiki', 'Pantai Sendiki merupakan salah satu wisata pantai yang berada di wilayah Tambakrejo, Sumbermanjing Wetan, Malang, Jawa Timur. Pantai ini bersebelahan dengan pantai Balekambang dan Pantai Sendang Biru.', 'kendaraan , tour guide , snorkle', 'tersedia', 400000, 'obwis', 'assets/wisata/sendiki.jpg', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `transaksiobwis`
--

CREATE TABLE `transaksiobwis` (
  `id_transaksi` int(11) NOT NULL,
  `id_wisata` int(11) NOT NULL,
  `id_member` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `total` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaksiobwis`
--

INSERT INTO `transaksiobwis` (`id_transaksi`, `id_wisata`, `id_member`, `tanggal`, `total`) VALUES
(19, 1, 3, '2017-12-13', 500000),
(21, 1, 3, '2017-12-13', 500000);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id_member`);

--
-- Indexes for table `objekwisata`
--
ALTER TABLE `objekwisata`
  ADD PRIMARY KEY (`id_wisata`);

--
-- Indexes for table `transaksiobwis`
--
ALTER TABLE `transaksiobwis`
  ADD PRIMARY KEY (`id_transaksi`),
  ADD KEY `id_member` (`id_member`),
  ADD KEY `id_obwis` (`id_wisata`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id_member` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `objekwisata`
--
ALTER TABLE `objekwisata`
  MODIFY `id_wisata` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `transaksiobwis`
--
ALTER TABLE `transaksiobwis`
  MODIFY `id_transaksi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `transaksiobwis`
--
ALTER TABLE `transaksiobwis`
  ADD CONSTRAINT `transaksiobwis_ibfk_1` FOREIGN KEY (`id_member`) REFERENCES `member` (`id_member`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `transaksiobwis_ibfk_2` FOREIGN KEY (`id_wisata`) REFERENCES `objekwisata` (`id_wisata`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
